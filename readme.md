I never faced such thing as realy flexible and reusable heroku buildpack. All of them are good only for project bootstrapping. Once, you will write project-specific buildpack and publish it as "myproject-heroku-buildpack".

The alternative idea is to use single buildpack for different stacks describing heroku-related tasks right in a project's Makefile.

So, how `BUILDPACK_URL=https://github.com/ilikefm/hub.git` works:

1. _Detect_ step just checks if `Makefile` exists in project root and exits otherwise.
2. _Compile_ step tries to reach `heroku` target in project's root passing `$1/$2/$3` into make vars, so they are available as `BUILD_DIR/CACHE_DIR/ENV_DIR`. `heroku` name is convension and can be used to run heroku-related scenarious like software installation, grabbing external dependencies, etc. Leave the target blank if you don't need it but want to avoid build cycle beeing interrupted due to make's "no such target" error.
3. _Release_ step optionally executes `make heroku-release` and always ends with zero status code not depending on target execution. You can print corresponding yaml with this phony. See [heroku documentation](https://devcenter.heroku.com/articles/buildpack-api) for more datails.

Example Makefile for golang project on cedar-14 stack:

```
# conditionally include .env file
# use .env to store only app-related vars which should be used in a Procfile too
-include .env
export

# if BUILD_DIR/CACHE_DIR are not set, fallback to default values
# on heroku, buildpack's bin/compile will rewrite them
BUILD_DIR ?= $(CURDIR)
CACHE_DIR ?= $(BUILD_DIR)/.tmp

# store godeps on the working dir by default
GODEPS = $(CURDIR)/.dep

# define which go version to use
GOVERSION = 1.4.2

# on heroku custom golang installation is used so we need to hack some vars and tune PATH a little bit
ifeq ($(STACK),cedar-14)
export GODEPS = $(CACHE_DIR)/godeps
export GOROOT := $(CACHE_DIR)/go/$(GOVERSION)
export PATH := $(GOROOT)/bin:$(PATH)
endif

# add BUILD_DIR to gopath
# both vars can not be rewrited outside of make
export GOPATH = $(BUILD_DIR):$(GODEPS)

bin: $(shell find src -type f -name "*.go")
    @ go install all
    @ go test `cd src && find * -type d`
    @ touch $@

.PHONY: heroku heroku-release get run
heroku:
    # start executing heroku compile...
    @ [ -d $(GOROOT) ] || ( \
        echo '-----> get and install golang into $(GOROOT)' && \
        mkdir -p $(GOROOT) && \
        curl -s https://storage.googleapis.com/golang/go$(GOVERSION).linux-amd64.tar.gz | tar zx -C $(GOROOT) && \
        mv $(GOROOT)/go/* $(GOROOT) && rm -rf $(GOROOT)/go \
        )
    @ $(MAKE) --no-print-directory get bin

heroku-release:
    @ echo 'default_process_types:'
    @ echo '  web: bin/server'

get:
    # get golang dependencies into $(GODEPS)
    @ mkdir -p $(GODEPS)
    @ GOPATH=$(GODEPS) go get -d -u `go list -f '{{join .Deps "\n"}}' ./... | xargs | sort | uniq`
    @ touch $(GODEPS)
    @ (cd $(GODEPS)/src && find * -type d -mindepth 2 -maxdepth 2) 2>/dev/null || echo "No external dependencies"
    @ cd $(BUILD_DIR)

run: bin
    @ bin/server
```
